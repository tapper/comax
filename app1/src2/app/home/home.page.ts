import { Component } from '@angular/core';
import { ListPage } from "../list/list.page";
import { ModalController } from "@ionic/angular";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  constructor(public modalCtrl:ModalController)
  {

  }

  //   async openModalpublic (){
  //     console.log('ModalEscualas');
  //     const modal = await this.modalCtrl.create({
  //         component: ListPage,
  //         componentProps: { value: 123 }
  //     });
  //     return await modal.present();       
  // }
}
