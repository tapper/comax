import { Firebase } from '@ionic-native/firebase/ngx';
import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private firebase: Firebase
  ) {

    // this.firebase.getToken()
    // .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
    // .catch(error => console.error('Error getting token', error));

    //   this.firebase.onNotificationOpen()
    //   .subscribe(data => console.log(`User opened a notification ${data}`));

    //   this.firebase.onTokenRefresh()
    //   .subscribe((token: string) => console.log(`Got a new token ${token}`));
      this.initializeApp();
   
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });


    if (this.platform.is('cordova')) {
      if (this.platform.is('android')) {
          this.initializeFireBaseAndroid();
      }
      // if (this.platform.is('ios')) {
      //     this.initializeFireBaseIos();
      // }
    }

  }

  private initializeFireBaseAndroid(): Promise<any> {
    return this.firebase.getToken()
        .catch(error =>
            console.error('Error getting token', error)
        )
        .then(token => {
            this.firebase.subscribe('all').then((result) => {
                if (result) console.log(`Subscribed to all`);
                this.subscribeToPushNotificationEvents();
            });
        });
  }


  private subscribeToPushNotificationEvents(): void {
        
    // Handle token refresh
    this.firebase.onTokenRefresh().subscribe(
        token => {},
        error => {
            console.error('Error refreshing token', error);
        });

    // Handle incoming notifications
    this.firebase.onNotificationOpen().subscribe(
        (notification) => {
            if (!notification.tap) {
    
            }
            else {
                setTimeout( () =>
                { 
                    if(notification.Type == "0" || notification.Type == "1")
                    {
                        //alert("1"+notification.ActivityDateID)
                        //this.nav.push(ShowPage,{'id':'ca33c997-dfdb-45ac-9e1c-b4adced78381','type':'2','grades':[]});
                        //this.nav.push(ShowPage,{'id':notification.ActivityDateID,'type':'2','grades':[]});
                    }
                    else if(notification.Type == "2")
                    {
                        //alert("2"+notification.ShowFeedbackID)
                        //this.nav.push(FeedbackPage,{activityDateID:notification.ShowFeedbackID});
                        //this.nav.push(FeedbackPage,{activityDateID:'de9da80f-f0c9-45ac-8ea4-03e77e7a881f'});
                    }
                          
                       // }
                    }, 3000);
                }
        },
        error => {
            console.error('Error getting the notification', error);
        });
}


}
