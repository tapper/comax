import { Component } from '@angular/core';
import { ModalController } from "@ionic/angular";
import { ListPage } from "../list/list.page";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(public modalCtrl:ModalController)
  {

  }

    async OpenModal (){
      console.log('ModalEscualas');
      const modal = await this.modalCtrl.create({
          component: ListPage,
          cssClass:'modalClass',
          componentProps: { value: 123 }
      });
      return await modal.present();       
  }
}
